import 'package:cinevision/domain/datasources/localstorage.datasource.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/domain/repositories/localstorage.repository.dart';

class LocalStorageImplRepository extends LocalStorageRepository {
  final LocalStorageDatasource datasource;

  LocalStorageImplRepository({
    required this.datasource,
  });

  @override
  Future<bool> isMovieFavorite(int id) {
    return datasource.isMovieFavorite(id);
  }

  @override
  Future<List<MovieEntity>> loadMoviesFavorite({
    int limit = 10,
    int offset = 0,
  }) {
    return datasource.loadMoviesFavorite(limit: limit, offset: offset);
  }

  @override
  Future<bool> toggleFavorite(MovieEntity movie) {
    return datasource.toggleFavorite(movie);
  }
}
