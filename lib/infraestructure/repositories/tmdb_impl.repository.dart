import 'package:cinevision/domain/datasources/tmdb.datasource.dart';
import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/domain/repositories/tmdb.repository.dart';

class TmdbImplRepository extends TmdbRepository {
  final TmdbDatasource datasource;

  TmdbImplRepository({
    required this.datasource,
  });

  @override
  Future<List<MovieEntity>> getBillboardMovies({int page = 1}) {
    return datasource.getBillboardMovies(page: page);
  }

  @override
  Future<List<MovieEntity>> getUpcomingMovies({int page = 1}) {
    return datasource.getUpcomingMovies(page: page);
  }

  @override
  Future<List<MovieEntity>> getPopularMovies({int page = 1}) {
    return datasource.getPopularMovies(page: page);
  }

  @override
  Future<List<MovieEntity>> getTopRatedMovies({int page = 1}) {
    return datasource.getTopRatedMovies(page: page);
  }

  @override
  Future<MovieEntity> getDetailMovieById(String id) {
    return datasource.getDetailMovieById(id);
  }

  @override
  Future<List<ActorsEntity>> getActorsMovie(String id) {
    return datasource.getActorsMovie(id);
  }

  @override
  Future<List<MovieEntity>> searchMovie(String query) {
    return datasource.searchMovie(query);
  }
}
