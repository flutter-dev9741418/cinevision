import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_actors.model.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_details.model.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_result.model.dart';

class MovieMapper {
  static MovieEntity tmdbToEntity(TmbdResult tmdbResult) {
    return MovieEntity(
      adult: tmdbResult.adult,
      backdropPath: tmdbResult.backdropPath.isNotEmpty
          ? 'https://image.tmdb.org/t/p/w500${tmdbResult.backdropPath}'
          : 'no-backdropPath',
      genreIds: tmdbResult.genreIds.map((e) => e.toString()).toList(),
      id: tmdbResult.id,
      originalLanguage: tmdbResult.originalLanguage,
      originalTitle: tmdbResult.originalTitle,
      overview: tmdbResult.overview,
      popularity: tmdbResult.popularity,
      posterPath: tmdbResult.posterPath.isNotEmpty
          ? 'https://image.tmdb.org/t/p/w500${tmdbResult.posterPath}'
          : 'no-poster',
      releaseDate: tmdbResult.releaseDate,
      title: tmdbResult.title,
      video: tmdbResult.video,
      voteAverage: tmdbResult.voteAverage,
      voteCount: tmdbResult.voteCount,
    );
  }

  static MovieEntity tmdbToEntityById(TmdbDetails tmdbResult) {
    return MovieEntity(
      adult: tmdbResult.adult,
      backdropPath: tmdbResult.backdropPath.isNotEmpty
          ? 'https://image.tmdb.org/t/p/w500${tmdbResult.backdropPath}'
          : 'no-backdropPath',
      genreIds: tmdbResult.genres.map((e) => e.name).toList(),
      id: tmdbResult.id,
      originalLanguage: tmdbResult.originalLanguage,
      originalTitle: tmdbResult.originalTitle,
      overview: tmdbResult.overview,
      popularity: tmdbResult.popularity,
      posterPath: tmdbResult.posterPath.isNotEmpty
          ? 'https://image.tmdb.org/t/p/w500${tmdbResult.posterPath}'
          : 'no-poster',
      releaseDate: tmdbResult.releaseDate,
      title: tmdbResult.title,
      video: tmdbResult.video,
      voteAverage: tmdbResult.voteAverage,
      voteCount: tmdbResult.voteCount,
    );
  }

  static ActorsEntity tmdbToEntityActorsMovie(Cast cast) {
    return ActorsEntity(
      id: cast.id,
      name: cast.name,
      profilePath: cast.profilePath.isNotEmpty
          ? 'https://image.tmdb.org/t/p/w500${cast.profilePath}'
          : 'no-profilepath',
      character: cast.character,
    );
  }
}
