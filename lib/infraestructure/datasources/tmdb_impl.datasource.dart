import 'package:cinevision/config/environment/environment.dart';
import 'package:cinevision/domain/datasources/tmdb.datasource.dart';
import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/infraestructure/mappers/movie.mapper.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_actors.model.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_details.model.dart';
import 'package:cinevision/infraestructure/models/tmdb/tmdb_response.model.dart';
import 'package:dio/dio.dart';

class TmdbImplDatasource extends TmdbDatasource {
  final dio = Dio(
    BaseOptions(
      baseUrl: "https://api.themoviedb.org/3",
      queryParameters: {
        "api_key": Environment.apiKeyTMDB,
        "language": Environment.language,
        "include_adult": false,
      },
    ),
  );

  List<MovieEntity> _jsonToMovies(Map<String, dynamic> json) {
    final tmdbResponse = TmdbResponse.fromJson(json);

    final List<MovieEntity> movies = tmdbResponse.results.map(
      (item) {
        return MovieMapper.tmdbToEntity(item);
      },
    ).toList();

    return movies;
  }

  @override
  Future<List<MovieEntity>> getBillboardMovies({int page = 1}) async {
    final response = await dio.get(
      "/movie/now_playing",
      queryParameters: {
        "page": page,
      },
    );

    return _jsonToMovies(response.data);
  }

  @override
  Future<List<MovieEntity>> getUpcomingMovies({int page = 1}) async {
    final response = await dio.get(
      "/movie/upcoming",
      queryParameters: {
        "page": page,
      },
    );

    return _jsonToMovies(response.data);
  }

  @override
  Future<List<MovieEntity>> getPopularMovies({int page = 1}) async {
    final response = await dio.get(
      "/movie/popular",
      queryParameters: {
        "page": page,
      },
    );

    return _jsonToMovies(response.data);
  }

  @override
  Future<List<MovieEntity>> getTopRatedMovies({int page = 1}) async {
    final response = await dio.get(
      "/movie/top_rated",
      queryParameters: {
        "page": page,
      },
    );

    return _jsonToMovies(response.data);
  }

  @override
  Future<MovieEntity> getDetailMovieById(String id) async {
    final response = await dio.get("/movie/$id");
    final movie = TmdbDetails.fromJson(response.data);
    return MovieMapper.tmdbToEntityById(movie);
  }

  @override
  Future<List<ActorsEntity>> getActorsMovie(String id) async {
    final response = await dio.get("/movie/$id/credits");
    final actorsResponse = TmdbActors.fromJson(response.data);

    List<ActorsEntity> actors = actorsResponse.cast
        .map((item) => MovieMapper.tmdbToEntityActorsMovie(item))
        .toList();

    return actors;
  }

  @override
  Future<List<MovieEntity>> searchMovie(String query) async {
    final response = await dio.get(
      "/search/movie",
      queryParameters: {
        "query": query,
      },
    );

    return _jsonToMovies(response.data);
  }
}
