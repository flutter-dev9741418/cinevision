import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/domain/datasources/localstorage.datasource.dart';
import 'package:flutter/foundation.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';

class LocalStorageImplDatasource extends LocalStorageDatasource {
  late Future<Isar> db;

  LocalStorageImplDatasource() {
    db = openDB();
  }

  Future<Isar> openDB() async {
    final dir = await getApplicationDocumentsDirectory();

    if (Isar.instanceNames.isEmpty) {
      return await Isar.open(
        [MovieEntitySchema],
        inspector: true,
        directory: dir.path,
      );
    }

    return Future.value(Isar.getInstance());
  }

  @override
  Future<bool> isMovieFavorite(int id) async {
    final isar = await db;
    final MovieEntity? isFavorite =
        await isar.movieEntitys.filter().idEqualTo(id).findFirst();
    return isFavorite != null;
  }

  @override
  Future<List<MovieEntity>> loadMoviesFavorite({
    int limit = 10,
    int offset = 0,
  }) async {
    final isar = await db;
    return isar.movieEntitys.where().offset(offset).limit(limit).findAll();
  }

  @override
  Future<bool> toggleFavorite(MovieEntity movie) async {
    final isar = await db;
    final movieExiste =
        await isar.movieEntitys.filter().idEqualTo(movie.id).findFirst();

    debugPrint("movieExiste -< ${movieExiste?.id} >-");
    if (movieExiste != null) {
      debugPrint("DELETE Isar");
      isar.writeTxnSync(() => isar.movieEntitys.deleteSync(movieExiste.uuid!));
      return false;
    }

    debugPrint("ADD Isar");
    isar.writeTxnSync(() => isar.movieEntitys.putSync(movie));
    return true;
  }
}
