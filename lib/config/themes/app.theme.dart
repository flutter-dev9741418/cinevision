import 'package:flutter/material.dart';

const Color _customColor = Color(0xFF73b72b);
const colorThemes = <Color>[
  _customColor,
  Colors.blue,
  Colors.tealAccent,
  Colors.amberAccent,
  Colors.pinkAccent,
  Colors.redAccent
];

class AppTheme {
  final int selectColor;
  final Brightness mode;

  AppTheme({this.selectColor = 1, this.mode = Brightness.light})
      : assert(selectColor >= 1 && selectColor <= colorThemes.length,
            "Los parametros requeridos son entre 1 y ${colorThemes.length}");

  ThemeData theme() => ThemeData(
        useMaterial3: true,
        colorSchemeSeed: colorThemes[selectColor - 1],
        brightness: mode,
      );
}
