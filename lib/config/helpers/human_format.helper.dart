import 'package:intl/intl.dart';

class HumanFormatHelper {
  static String humanReadbleNumber({required double number, int decimals = 0}) {
    final formatterNumber = NumberFormat.currency(
      decimalDigits: decimals,
      symbol: '',
      locale: 'en_US',
    ).format(number);

    return formatterNumber;
  }
}
