import 'package:flutter_dotenv/flutter_dotenv.dart';

class Environment {
  static String apiKeyTMDB =
      dotenv.env['API_KEY_TMDB'] ?? 'I need api key The movie db';
  static String language = dotenv.env['LOCATE'] ?? 'es-MX';
}
