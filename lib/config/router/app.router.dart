import 'package:go_router/go_router.dart';
import 'package:cinevision/presentation/screens/screens.module.dart';

final appRouter = GoRouter(
  initialLocation: '/0',
  routes: [
    GoRoute(
      name: HomeScreen.name,
      path: '/:page',
      builder: (context, state) {
        final pageIndex = int.parse(state.pathParameters['page'] ?? '0');

        return HomeScreen(
          pageIndex: pageIndex,
        );
      },
      routes: [
        GoRoute(
          name: MovieScreen.name,
          path: 'movie/:id',
          builder: (context, state) {
            return MovieScreen(id: state.pathParameters['id'] ?? 'no-id');
          },
        ),
      ],
    ),
  ],
);
