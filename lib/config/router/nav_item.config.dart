import 'package:flutter/material.dart';

class NavItemConfig {
  final String title;
  final IconData icon;
  final String route;

  const NavItemConfig({
    required this.title,
    required this.icon,
    required this.route,
  });
}

const appNavItems = <NavItemConfig>[
  NavItemConfig(
    title: "Home",
    icon: Icons.home,
    route: "/",
  ),
  NavItemConfig(
    title: "Movie",
    icon: Icons.home,
    route: "/movie/:id",
  ),
];
