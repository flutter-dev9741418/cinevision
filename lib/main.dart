import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:cinevision/config/themes/app.theme.dart';
import 'package:cinevision/config/router/app.router.dart';
import 'package:cinevision/presentation/providers/provider.module.dart';

Future<void> main() async {
  await dotenv.load(fileName: ".env");

  runApp(const ProviderScope(
    child: MainApp(),
  ));
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: AppTheme(
        selectColor: ref.watch(selectedColorThemeProvider),
        mode:
            ref.watch(isDarkModeProvider) ? Brightness.dark : Brightness.light,
      ).theme(),
      routerConfig: appRouter,
    );
  }
}
