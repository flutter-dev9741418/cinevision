import 'package:animate_do/animate_do.dart';
import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/presentation/providers/provider.module.dart';
import 'package:cinevision/presentation/widgets/widget.module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MovieScreen extends StatelessWidget {
  static const name = "MovieScreen";

  final String id;

  const MovieScreen({
    super.key,
    required this.id,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _MovieView(
        id: id,
      ),
    );
  }
}

class _MovieView extends ConsumerStatefulWidget {
  final String id;
  const _MovieView({
    required this.id,
  });

  @override
  _MovieViewState createState() => _MovieViewState();
}

class _MovieViewState extends ConsumerState<_MovieView> {
  @override
  void initState() {
    super.initState();
    ref.read(movieDetailsProvider.notifier).loadMovie(widget.id);
    ref.read(actorsMovieProvider.notifier).loadActors(widget.id);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MovieEntity? movie = ref.watch(movieDetailsProvider)[widget.id];
    final List<ActorsEntity>? actors =
        ref.watch(actorsMovieProvider)[widget.id];

    return movie == null
        ? const FullscreenLoaderWidget()
        : CustomScrollView(
            physics: const ClampingScrollPhysics(),
            slivers: [
              _AppBar(movie: movie),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) =>
                      _MovieDetails(movie: movie, actors: actors ?? []),
                  childCount: 1,
                ),
              ),
            ],
          );
  }
}

final isFavoriteProvider = FutureProvider.family.autoDispose((ref, int id) {
  return ref.watch(localStorageRepositoryProvider).isMovieFavorite(id);
});

class _AppBar extends ConsumerWidget {
  final MovieEntity movie;

  const _AppBar({
    required this.movie,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final colors = Theme.of(context).colorScheme;
    final isFavorite = ref.watch(isFavoriteProvider(movie.id));

    return SliverAppBar(
      backgroundColor: Colors.black,
      expandedHeight: size.height * 0.7,
      foregroundColor: Colors.white,
      actions: [
        IconButton(
          onPressed: () async {
            await ref
                .read(favoriteMoviesProvider.notifier)
                .toggleFavorite(movie);
            ref.invalidate(isFavoriteProvider(movie.id));
          },
          icon: isFavorite.when(
            data: (data) {
              debugPrint("FAVORITE: ${movie.title} -> $data");
              return data
                  ? const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    )
                  : const Icon(Icons.favorite_border_rounded);
            },
            error: (_, __) => throw UnimplementedError(),
            loading: () => const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          ),
        ),
      ],
      flexibleSpace: FlexibleSpaceBar(
        titlePadding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        title: Text(
          movie.title,
          style: const TextStyle(
            fontSize: 16,
          ),
          textWidthBasis: TextWidthBasis.parent,
          textAlign: TextAlign.start,
        ),
        background: Stack(
          children: [
            SizedBox.expand(
              child: movie.posterPath == 'no-poster'
                  ? Container(
                      color: colors.onSurfaceVariant.withAlpha(20),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.image_outlined,
                              size: 55,
                              color: colors.onSurface.withAlpha(60),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              "No poster",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                                color: colors.onSurface.withAlpha(60),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : Image.network(
                      movie.posterPath,
                      fit: BoxFit.cover,
                      loadingBuilder: (context, child, loadingProgress) {
                        if (loadingProgress != null) {
                          return const DecoratedBox(
                            decoration: BoxDecoration(
                              color: Colors.black12,
                            ),
                            child: SizedBox(
                              child: Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                ),
                              ),
                            ),
                          );
                        }

                        return FadeIn(child: child);
                      },
                    ),
            ),
            const SizedBox.expand(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.7, 1.0],
                    colors: [
                      Colors.transparent,
                      Colors.black87,
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox.expand(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.0, 0.3],
                    colors: [
                      Colors.black87,
                      Colors.transparent,
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _MovieDetails extends StatelessWidget {
  final MovieEntity movie;
  final List<ActorsEntity> actors;

  const _MovieDetails({
    required this.movie,
    required this.actors,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final colors = Theme.of(context).colorScheme;
    final textStyle = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: movie.posterPath == 'no-poster'
                    ? SizedBox(
                        width: size.width * 0.3,
                        height: 220,
                        child: Container(
                          color: colors.onSurfaceVariant.withAlpha(20),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.image_outlined,
                                  size: 55,
                                  color: colors.onSurface.withAlpha(60),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "No poster",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w300,
                                    color: colors.onSurface.withAlpha(60),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    : Image.network(
                        movie.posterPath,
                        width: size.width * 0.3,
                        height: 220,
                        fit: BoxFit.cover,
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress != null) {
                            return const DecoratedBox(
                              decoration: BoxDecoration(
                                color: Colors.black12,
                              ),
                              child: SizedBox(
                                height: 220,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            );
                          }

                          return FadeIn(child: child);
                        },
                      ),
              ),
              const SizedBox(
                width: 10,
              ),
              SizedBox(
                width: (size.width * 0.7 - (10 + 20)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.title,
                      style: textStyle.titleLarge,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      movie.overview,
                      style: textStyle.bodyMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Wrap(
            children: [
              ...movie.genreIds
                  .map(
                    (item) => Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: Chip(
                        label: Text(item),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
        SizedBox(
          height: 325,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemCount: actors.length,
            itemBuilder: (context, index) {
              final ActorsEntity actor = actors[index];
              return Container(
                padding: const EdgeInsets.all(10),
                width: 135,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FadeInRight(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: actor.profilePath == 'no-profilepath'
                            ? SizedBox(
                                width: size.width * 0.3,
                                height: 220,
                                child: Container(
                                  color: colors.onSurfaceVariant.withAlpha(20),
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.person,
                                          size: 55,
                                          color: colors.onSurface.withAlpha(60),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "No photo",
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w300,
                                            color:
                                                colors.onSurface.withAlpha(60),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : Image.network(
                                actor.profilePath,
                                width: size.width * 0.3,
                                height: 220,
                                fit: BoxFit.cover,
                                loadingBuilder:
                                    (context, child, loadingProgress) {
                                  if (loadingProgress != null) {
                                    return const DecoratedBox(
                                      decoration: BoxDecoration(
                                        color: Colors.black12,
                                      ),
                                      child: SizedBox(
                                        height: 220,
                                        child: Center(
                                          child: CircularProgressIndicator(
                                            strokeWidth: 2,
                                          ),
                                        ),
                                      ),
                                    );
                                  }

                                  return FadeIn(child: child);
                                },
                              ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 10,
                        top: 10,
                        right: 10,
                      ),
                      child: Text(
                        actor.name,
                        style: textStyle.titleSmall,
                        textAlign: TextAlign.start,
                        maxLines: 2,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 10,
                        top: 5,
                        right: 10,
                      ),
                      child: Text(
                        actor.character ?? '',
                        style: textStyle.bodySmall,
                        textAlign: TextAlign.start,
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }
}
