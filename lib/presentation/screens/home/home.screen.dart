import 'package:flutter/material.dart';

import 'package:cinevision/presentation/widgets/widget.module.dart';
import 'package:cinevision/presentation/views/views.module.dart';

class HomeScreen extends StatelessWidget {
  static const name = "HomeScreen";
  final int pageIndex;

  HomeScreen({
    super.key,
    required this.pageIndex,
  });

  final List<Widget> viewRoutes = [
    const HomeView(),
    const CategoryView(),
    const FavoriteView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: pageIndex,
        children: viewRoutes,
      ),
      bottomNavigationBar: NavbarBottomWidget(
        currentIndex: pageIndex,
      ),
    );
  }
}
