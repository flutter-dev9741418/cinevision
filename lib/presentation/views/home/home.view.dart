import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cinevision/presentation/widgets/widget.module.dart';
import 'package:cinevision/presentation/providers/provider.module.dart';

class HomeView extends ConsumerStatefulWidget {
  static const name = "HomeView";

  const HomeView({
    super.key,
  });

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends ConsumerState<HomeView>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    ref.read(billboardMoviesProvider.notifier).loadNextPage();
    ref.read(upcomingMoviesProvider.notifier).loadNextPage();
    ref.read(popularMoviesProvider.notifier).loadNextPage();
    ref.read(topRatedMoviesProvider.notifier).loadNextPage();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final moviesSlideshow = ref.watch(moviesSlideshowProvider);
    final billboardMovies = ref.watch(billboardMoviesProvider);
    final upcomingMovies = ref.watch(upcomingMoviesProvider);
    final popularMovies = ref.watch(popularMoviesProvider);
    final topRatedMovies = ref.watch(topRatedMoviesProvider);

    final isLoading = ref.watch(initLoadingProvider);

    return isLoading
        ? const FullscreenLoaderWidget()
        : CustomScrollView(
            slivers: [
              const SliverAppBar(
                floating: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: SidebarWidget(),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return Column(
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        MoviesSlideShowWidget(movies: moviesSlideshow),
                        const SizedBox(
                          height: 20,
                        ),
                        MoviesLiveviewWidget(
                          movies: billboardMovies,
                          title: "En cines",
                          loadNextPage: () {
                            ref
                                .read(billboardMoviesProvider.notifier)
                                .loadNextPage();
                          },
                        ),
                        MoviesLiveviewWidget(
                          movies: upcomingMovies,
                          title: "Próximamente",
                          loadNextPage: () {
                            ref
                                .read(upcomingMoviesProvider.notifier)
                                .loadNextPage();
                          },
                        ),
                        MoviesLiveviewWidget(
                          movies: popularMovies,
                          title: "Populares",
                          loadNextPage: () {
                            ref
                                .read(popularMoviesProvider.notifier)
                                .loadNextPage();
                          },
                        ),
                        MoviesLiveviewWidget(
                          movies: topRatedMovies,
                          title: "Mejores calificadas",
                          subtitle: "Desde siempre",
                          loadNextPage: () {
                            ref
                                .read(topRatedMoviesProvider.notifier)
                                .loadNextPage();
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    );
                  },
                  childCount: 1,
                ),
              ),
            ],
          );
  }

  @override
  bool get wantKeepAlive => true;
}
