import 'package:cinevision/presentation/providers/provider.module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../widgets/widget.module.dart';

class FavoriteView extends ConsumerStatefulWidget {
  static const name = "FavoriteView";

  const FavoriteView({
    super.key,
  });

  @override
  FavoriteViewState createState() => FavoriteViewState();
}

class FavoriteViewState extends ConsumerState<FavoriteView>
    with AutomaticKeepAliveClientMixin {
  bool isLastPage = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    loadNextPage();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void loadNextPage() async {
    if (isLoading || isLastPage) return;
    isLoading = true;

    final movies =
        await ref.read(favoriteMoviesProvider.notifier).loadNextPage();
    isLoading = false;

    if (movies.isEmpty) {
      isLastPage = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    // Transformar map a array .values.toList();
    final favoriteMovies = ref.watch(favoriteMoviesProvider).values.toList();
    final colors = Theme.of(context).colorScheme;

    return favoriteMovies.isEmpty
        ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.favorite_rounded,
                  size: 70,
                  color: colors.onSurface.withOpacity(.25),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "Oooh no!!",
                  style: TextStyle(
                    fontSize: 40,
                    color: colors.onSurface.withOpacity(.25),
                  ),
                ),
                Text(
                  "No tienes películas favoritas",
                  style: TextStyle(
                    fontSize: 18,
                    color: colors.onSurface.withOpacity(.25),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                FilledButton.tonal(
                  onPressed: () => context.go("/0"),
                  child: const Text("Agregar películas"),
                ),
              ],
            ),
          )
        : MovieMasonryWidget(
            movies: favoriteMovies,
            loadNextPage: loadNextPage,
          );
  }

  @override
  bool get wantKeepAlive => true;
}
