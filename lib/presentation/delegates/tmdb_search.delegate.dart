import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:cinevision/config/helpers/human_format.helper.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:flutter/material.dart';

typedef SearchMoviesCallback = Future<List<MovieEntity>> Function(String query);

class TmdbSearchDelegate extends SearchDelegate<MovieEntity?> {
  final SearchMoviesCallback searchMovies;
  List<MovieEntity> initialMovies;

  StreamController<List<MovieEntity>> debounceMovies =
      StreamController.broadcast();
  StreamController<bool> loading = StreamController.broadcast();

  Timer? _debounceTimer;

  TmdbSearchDelegate({
    required this.initialMovies,
    required this.searchMovies,
  });

  void clearStreams() => debounceMovies.close();

  void _onQueryChanged(String query) {
    loading.add(true);
    if (_debounceTimer?.isActive ?? false) _debounceTimer!.cancel();
    _debounceTimer = Timer(const Duration(milliseconds: 500), () async {
      final movies = await searchMovies(query);
      debounceMovies.add(movies);
      initialMovies = movies;
      loading.add(false);
    });
  }

  Widget buildResultAndSuggestions() {
    return StreamBuilder(
      initialData: initialMovies,
      stream: debounceMovies.stream,
      builder: (context, snapshot) {
        final movies = snapshot.data ?? [];
        return ListView.builder(
          itemCount: movies.length,
          itemBuilder: (context, index) => _MovieItemWidget(
            movie: movies[index],
            onClick: () {
              clearStreams();
              close(context, movies[index]);
            },
          ),
        );
      },
    );
  }

  @override
  String get searchFieldLabel => "Buscar";

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      StreamBuilder(
        initialData: false,
        stream: loading.stream,
        builder: (context, snapshot) {
          return snapshot.data ?? false
              ? SpinPerfect(
                  infinite: true,
                  spins: 10,
                  duration: const Duration(
                    seconds: 10,
                  ),
                  child: const IconButton(
                    onPressed: null,
                    icon: Icon(
                      Icons.refresh_rounded,
                    ),
                  ),
                )
              : FadeIn(
                  animate: query.isNotEmpty,
                  duration: const Duration(
                    milliseconds: 200,
                  ),
                  child: IconButton(
                    onPressed: () => query = "",
                    icon: const Icon(
                      Icons.clear,
                    ),
                  ),
                );
        },
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        clearStreams();
        close(context, null);
      },
      icon: const Icon(
        Icons.arrow_back_ios_new_rounded,
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: buildResultAndSuggestions(),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    _onQueryChanged(query);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: buildResultAndSuggestions(),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

class _MovieItemWidget extends StatelessWidget {
  final MovieEntity movie;
  final VoidCallback onClick;

  const _MovieItemWidget({
    required this.movie,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final colors = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;

    const int titleLength = 38;
    const int descriptionLength = 200;

    return GestureDetector(
      onTap: onClick,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          top: 10,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: size.width * 0.15,
              height: 95,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: movie.posterPath == 'no-poster'
                    ? SizedBox(
                        width: size.width * 0.15,
                        height: 95,
                        child: Container(
                          color: colors.onSurfaceVariant.withAlpha(20),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.image_outlined,
                                  size: 16,
                                  color: colors.onSurface.withAlpha(95),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Image.network(
                        movie.posterPath,
                        width: size.width * 0.15,
                        height: 95,
                        fit: BoxFit.cover,
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress != null) {
                            return const DecoratedBox(
                              decoration: BoxDecoration(
                                color: Colors.black12,
                              ),
                              child: SizedBox(
                                height: 95,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            );
                          }

                          return FadeIn(child: child);
                        },
                      ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            SizedBox(
              width: (size.width * 0.85) - 30,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title.length >= titleLength
                        ? '${movie.title.substring(0, titleLength)}...'
                        : movie.title,
                    style: textTheme.titleMedium,
                  ),
                  if (movie.overview.isNotEmpty)
                    const SizedBox(
                      height: 5,
                    ),
                  if (movie.overview.isNotEmpty)
                    Text(
                      movie.overview.length >= descriptionLength
                          ? '${movie.overview.substring(0, descriptionLength)}...'
                          : movie.overview,
                      style: textTheme.bodySmall,
                    ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: SizedBox(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.star_half_rounded,
                            color: Colors.yellow.shade800,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            HumanFormatHelper.humanReadbleNumber(
                              number: movie.voteAverage,
                              decimals: 1,
                            ),
                            style: textTheme.bodyMedium?.copyWith(
                              color: Colors.yellow.shade800,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: 1,
                            height: 15,
                            color: colors.onSurface,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: Text(
                              HumanFormatHelper.humanReadbleNumber(
                                number: movie.popularity,
                              ),
                              style: textTheme.bodyMedium,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
