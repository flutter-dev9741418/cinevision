import 'package:animate_do/animate_do.dart';
import 'package:cinevision/config/helpers/human_format.helper.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class MovieWidget extends StatelessWidget {
  final MovieEntity movie;
  final bool onlyPoster;

  const MovieWidget({
    super.key,
    required this.movie,
    this.onlyPoster = false,
  });

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final titleStyle = Theme.of(context).textTheme;

    return Container(
      padding: const EdgeInsets.only(top: 10),
      margin: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 150,
            height: onlyPoster ? 180 : 240,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: movie.posterPath == 'no-poster'
                  ? Container(
                      color: colors.onSurfaceVariant.withAlpha(20),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.image_outlined,
                              size: 55,
                              color: colors.onSurface.withAlpha(60),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              "No poster",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                                color: colors.onSurface.withAlpha(60),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : Image.network(
                      movie.posterPath,
                      width: 150,
                      height: onlyPoster ? 180 : 240,
                      fit: BoxFit.cover,
                      loadingBuilder: (context, child, loadingProgress) {
                        if (loadingProgress != null) {
                          return const DecoratedBox(
                            decoration: BoxDecoration(
                              color: Colors.black12,
                            ),
                            child: SizedBox(
                              height: 240,
                              child: Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                ),
                              ),
                            ),
                          );
                        }

                        return GestureDetector(
                          onTap: () => context.go("/0/movie/${movie.id}"),
                          child: FadeIn(child: child),
                        );
                      },
                    ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          if (!onlyPoster)
            SizedBox(
              width: 150,
              height: 38,
              child: Text(
                movie.title,
                maxLines: 2,
                style: titleStyle.titleSmall,
              ),
            ),
          if (!onlyPoster)
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: SizedBox(
                width: 150,
                child: Row(
                  children: [
                    Icon(
                      Icons.star_half_rounded,
                      color: Colors.yellow.shade800,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      HumanFormatHelper.humanReadbleNumber(
                        number: movie.voteAverage,
                        decimals: 1,
                      ),
                      style: titleStyle.bodyMedium?.copyWith(
                        color: Colors.yellow.shade800,
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Text(
                        HumanFormatHelper.humanReadbleNumber(
                          number: movie.popularity,
                        ),
                        style: titleStyle.bodyMedium,
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
