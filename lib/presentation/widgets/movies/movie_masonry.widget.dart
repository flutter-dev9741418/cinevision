import 'package:animate_do/animate_do.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/presentation/widgets/movies/movie.widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/material.dart';

class MovieMasonryWidget extends StatefulWidget {
  final List<MovieEntity> movies;
  final VoidCallback? loadNextPage;

  const MovieMasonryWidget({
    super.key,
    required this.movies,
    required this.loadNextPage,
  });

  @override
  State<MovieMasonryWidget> createState() => _MovieMasonryWidgetState();
}

class _MovieMasonryWidgetState extends State<MovieMasonryWidget> {
  final scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      if (widget.loadNextPage == null) return;

      if ((scrollController.position.pixels + 200) >=
          scrollController.position.maxScrollExtent) {
        widget.loadNextPage!();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: MasonryGridView.count(
        controller: scrollController,
        crossAxisCount: 3,
        itemCount: widget.movies.length,
        itemBuilder: (context, index) {
          return FadeInUp(
            child: MovieWidget(
              movie: widget.movies[index],
              onlyPoster: true,
            ),
          );
        },
      ),
    );
  }
}
