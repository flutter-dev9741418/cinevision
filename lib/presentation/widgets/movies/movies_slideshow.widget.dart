import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:go_router/go_router.dart';

class MoviesSlideShowWidget extends StatelessWidget {
  final List<MovieEntity> movies;

  const MoviesSlideShowWidget({
    super.key,
    required this.movies,
  });

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    return SizedBox(
      width: double.infinity,
      height: 210,
      child: Swiper(
        viewportFraction: 0.8,
        scale: 0.9,
        pagination: SwiperPagination(
          margin: const EdgeInsets.only(top: 10),
          builder: DotSwiperPaginationBuilder(
            activeColor: colors.primary,
            color: colors.secondary,
          ),
        ),
        autoplay: true,
        itemCount: movies.length,
        itemBuilder: (context, index) => _Slide(movie: movies[index]),
      ),
    );
  }
}

class _Slide extends StatelessWidget {
  final MovieEntity movie;

  const _Slide({
    required this.movie,
  });

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    final decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(20),
    );

    return Padding(
      padding: const EdgeInsets.only(
        bottom: 30,
      ),
      child: DecoratedBox(
        decoration: decoration,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: movie.backdropPath == 'no-backdropPath'
              ? Container(
                  color: colors.onSurfaceVariant.withAlpha(20),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.image_outlined,
                          size: 55,
                          color: colors.onSurface.withAlpha(60),
                        ),
                      ],
                    ),
                  ),
                )
              : Image.network(
                  movie.backdropPath,
                  fit: BoxFit.cover,
                  loadingBuilder: (context, child, loadingProgress) {
                    if (loadingProgress != null) {
                      return const DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black12,
                        ),
                      );
                    }

                    return GestureDetector(
                      onTap: () => context.go("/0/movie/${movie.id}"),
                      child: FadeIn(child: child),
                    );
                  },
                ),
        ),
      ),
    );
  }
}
