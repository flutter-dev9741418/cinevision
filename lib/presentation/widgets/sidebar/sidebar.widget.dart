import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/presentation/providers/provider.module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cinevision/presentation/delegates/tmdb_search.delegate.dart';
import 'package:go_router/go_router.dart';

class SidebarWidget extends ConsumerWidget {
  const SidebarWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final colors = Theme.of(context).colorScheme;

    return SafeArea(
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 5,
        ),
        child: SizedBox(
          width: double.infinity,
          child: Row(
            children: [
              Icon(
                Icons.movie_creation_rounded,
                color: colors.primary,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                "Cinevision",
                style: TextStyle(
                  fontSize: 20,
                  color: colors.onSurface,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const Spacer(),
              IconButton(
                onPressed: () {
                  showSearch<MovieEntity?>(
                    query: ref.watch(searchQueryProvider),
                    context: context,
                    delegate: TmdbSearchDelegate(
                        initialMovies: ref.read(searchMovieProvider),
                        searchMovies: ref
                            .read(searchMovieProvider.notifier)
                            .searchMoviesByQuery),
                  ).then((item) {
                    if (item == null) return;
                    context.push("/0/movie/${item.id}");
                  });
                },
                icon: const Icon(
                  Icons.search_rounded,
                ),
              ),
              IconButton(
                onPressed: () {
                  ref
                      .read(isDarkModeProvider.notifier)
                      .update((state) => !state);
                },
                icon: ref.watch(isDarkModeProvider)
                    ? const Icon(Icons.wb_sunny_rounded)
                    : const Icon(Icons.dark_mode_rounded),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
