import 'package:flutter/material.dart';

class FullscreenLoaderWidget extends StatelessWidget {
  const FullscreenLoaderWidget({super.key});

  Stream<String> getLoaderMessage() {
    final message = <String>[
      "Cargando películas",
      "Comprando palomitas de maíz",
      "Cargando populares",
      "Llamando a mi novia",
      "Ya mero...",
      "Esto está tardando mas de lo esperado 😥"
    ];

    return Stream.periodic(const Duration(milliseconds: 1600), (step) {
      return message[step];
    }).take(message.length);
  }

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Cinevision",
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.w400,
              color: colors.onSurface.withAlpha(100),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          const CircularProgressIndicator(
            strokeWidth: 2,
          ),
          const SizedBox(
            height: 40,
          ),
          StreamBuilder(
            stream: getLoaderMessage(),
            builder: (context, snapshot) {
              return Text(
                !snapshot.hasData ? "Cargando..." : snapshot.data!,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  color: colors.onSurface.withAlpha(90),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
