import "package:cinevision/infraestructure/datasources/localstorage_impl.datasource.dart";
import "package:cinevision/infraestructure/repositories/localstorage_impl.repository.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";

final localStorageRepositoryProvider = Provider((ref) {
  return LocalStorageImplRepository(datasource: LocalStorageImplDatasource());
});
