import "package:cinevision/domain/entities/movie.entity.dart";
import "package:cinevision/presentation/providers/localstorage/localstorage.provider.dart";
import "package:flutter/foundation.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";

final favoriteMoviesProvider =
    StateNotifierProvider<FavoriteMoviesNotifier, Map<int, MovieEntity>>(
  (ref) => FavoriteMoviesNotifier(
    funFavoriteMovies:
        ref.watch(localStorageRepositoryProvider).loadMoviesFavorite,
    ref: ref,
  ),
);

typedef FavoriteMoviesCallback = Future<List<MovieEntity>> Function(
    {int limit, int offset});

class FavoriteMoviesNotifier extends StateNotifier<Map<int, MovieEntity>> {
  final FavoriteMoviesCallback funFavoriteMovies;
  final Ref ref;

  int page = 0;
  int limit = 12;

  FavoriteMoviesNotifier({
    required this.funFavoriteMovies,
    required this.ref,
  }) : super({});

  Future<List<MovieEntity>> loadNextPage() async {
    final movies = await funFavoriteMovies(
      offset: page * limit,
      limit: limit,
    );
    page++;

    final temp = <int, MovieEntity>{};
    for (final movie in movies) {
      temp[movie.id] = movie;
    }

    state = {...state, ...temp};
    return movies;
  }

  Future<void> toggleFavorite(MovieEntity movie) async {
    final isMovieInFavorite =
        await ref.watch(localStorageRepositoryProvider).toggleFavorite(movie);

    debugPrint("toggleFavorite -< $isMovieInFavorite >-");

    if (isMovieInFavorite) {
      state = {...state, movie.id: movie};
    } else {
      state.remove(movie.id);
      state = {...state};
    }
  }
}
