import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cinevision/config/themes/app.theme.dart';

final isDarkModeProvider = StateProvider<bool>((ref) => false);
final selectedColorThemeProvider = StateProvider<int>((ref) => 1);
final selectedColorProvider = StateProvider((ref) => 0);
final colorListProvider = Provider((ref) => colorThemes);
