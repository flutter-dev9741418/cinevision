import 'package:cinevision/presentation/providers/tmdb/tmdbs.provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final initLoadingProvider = Provider<bool>((ref) {
  final billboardMovies = ref.watch(billboardMoviesProvider).isEmpty;
  final upcomingMovies = ref.watch(upcomingMoviesProvider).isEmpty;
  final popularMovies = ref.watch(popularMoviesProvider).isEmpty;
  final topRatedMovies = ref.watch(topRatedMoviesProvider).isEmpty;

  return billboardMovies || upcomingMovies || popularMovies || topRatedMovies
      ? true
      : false;
});
