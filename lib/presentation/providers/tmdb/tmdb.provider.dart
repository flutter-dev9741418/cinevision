import 'package:cinevision/infraestructure/datasources/tmdb_impl.datasource.dart';
import 'package:cinevision/infraestructure/repositories/tmdb_impl.repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final tmdbRepositoryProvider = Provider(
  (ref) => TmdbImplRepository(
    datasource: TmdbImplDatasource(),
  ),
);
