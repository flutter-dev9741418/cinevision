import 'package:cinevision/presentation/providers/tmdb/tmdb.provider.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final searchQueryProvider = StateProvider<String>((ref) => "");

final searchMovieProvider =
    StateNotifierProvider<SearchMovieNotifier, List<MovieEntity>>(
  (ref) => SearchMovieNotifier(
    funSearchMovie: ref.watch(tmdbRepositoryProvider).searchMovie,
    ref: ref,
  ),
);

typedef SearchMoviesCallback = Future<List<MovieEntity>> Function(String query);

class SearchMovieNotifier extends StateNotifier<List<MovieEntity>> {
  final SearchMoviesCallback funSearchMovie;
  final Ref ref;

  SearchMovieNotifier({
    required this.funSearchMovie,
    required this.ref,
  }) : super([]);

  Future<List<MovieEntity>> searchMoviesByQuery(String query) async {
    List<MovieEntity> movies = [];
    ref.read(searchQueryProvider.notifier).update((state) => query);

    if (query.isNotEmpty) {
      movies = await funSearchMovie(query);
    }

    state = movies;

    return movies;
  }
}
