import 'package:cinevision/presentation/providers/tmdb/tmdb.provider.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final movieDetailsProvider =
    StateNotifierProvider<MovieDetailsNotifier, Map<String, dynamic>>(
  (ref) => MovieDetailsNotifier(
    funMovieDetails: (id) {
      return ref.watch(tmdbRepositoryProvider).getDetailMovieById(id);
    },
  ),
);

typedef GetMovieDetailsCallback = Future<MovieEntity> Function(String id);

class MovieDetailsNotifier extends StateNotifier<Map<String, dynamic>> {
  final GetMovieDetailsCallback funMovieDetails;
  bool isLoading = false;

  MovieDetailsNotifier({
    required this.funMovieDetails,
  }) : super({});

  Future<void> loadMovie(String id) async {
    if (state[id] != null) return;
    if (isLoading) return;
    isLoading = true;
    final movie = await funMovieDetails(id);
    state = {...state, id: movie};
    await Future.delayed(const Duration(milliseconds: 300));
    isLoading = false;
  }
}
