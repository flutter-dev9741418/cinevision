import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/presentation/providers/tmdb/tmdb.provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final actorsMovieProvider =
    StateNotifierProvider<ActorsMovieNotifier, Map<String, List<ActorsEntity>>>(
  (ref) => ActorsMovieNotifier(
    funActorsMovie: (id) {
      return ref.watch(tmdbRepositoryProvider).getActorsMovie(id);
    },
  ),
);

typedef GetActorsMovieCallback = Future<List<ActorsEntity>> Function(String id);

class ActorsMovieNotifier
    extends StateNotifier<Map<String, List<ActorsEntity>>> {
  final GetActorsMovieCallback funActorsMovie;
  bool isLoading = false;

  ActorsMovieNotifier({
    required this.funActorsMovie,
  }) : super({});

  Future<void> loadActors(String id) async {
    if (state[id] != null) return;
    if (isLoading) return;
    isLoading = true;
    final actors = await funActorsMovie(id);
    state = {...state, id: actors};
    await Future.delayed(const Duration(milliseconds: 300));
    isLoading = false;
  }
}
