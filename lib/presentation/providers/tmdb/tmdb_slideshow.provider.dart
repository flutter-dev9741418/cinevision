import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/presentation/providers/provider.module.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final moviesSlideshowProvider = Provider<List<MovieEntity>>((ref) {
  final billboardMovies = ref.watch(billboardMoviesProvider);

  if (billboardMovies.isEmpty) return [];
  return billboardMovies.sublist(0, 5);
});
