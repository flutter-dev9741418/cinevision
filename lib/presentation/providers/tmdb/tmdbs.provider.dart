import 'package:cinevision/domain/entities/movie.entity.dart';
import 'package:cinevision/presentation/providers/tmdb/tmdb.provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final billboardMoviesProvider =
    StateNotifierProvider<TmdbNotifier, List<MovieEntity>>(
  (ref) => TmdbNotifier(
    fecthMoreMovies: ref.watch(tmdbRepositoryProvider).getBillboardMovies,
  ),
);

final upcomingMoviesProvider =
    StateNotifierProvider<TmdbNotifier, List<MovieEntity>>(
  (ref) => TmdbNotifier(
    fecthMoreMovies: ref.watch(tmdbRepositoryProvider).getUpcomingMovies,
  ),
);

final popularMoviesProvider =
    StateNotifierProvider<TmdbNotifier, List<MovieEntity>>(
  (ref) => TmdbNotifier(
    fecthMoreMovies: ref.watch(tmdbRepositoryProvider).getPopularMovies,
  ),
);

final topRatedMoviesProvider =
    StateNotifierProvider<TmdbNotifier, List<MovieEntity>>(
  (ref) => TmdbNotifier(
    fecthMoreMovies: ref.watch(tmdbRepositoryProvider).getTopRatedMovies,
  ),
);

typedef MovieCallback = Future<List<MovieEntity>> Function({int page});

class TmdbNotifier extends StateNotifier<List<MovieEntity>> {
  int currentPage = 0;
  MovieCallback fecthMoreMovies;
  bool isLoading = false;

  TmdbNotifier({required this.fecthMoreMovies}) : super([]);

  Future<void> loadNextPage() async {
    if (isLoading) return;
    isLoading = true;
    currentPage++;
    final List<MovieEntity> movies = await fecthMoreMovies(page: currentPage);
    state = [...state, ...movies];
    await Future.delayed(const Duration(milliseconds: 300));
    isLoading = false;
  }
}
