import 'package:cinevision/domain/entities/actors.entity.dart';
import 'package:cinevision/domain/entities/movie.entity.dart';

abstract class TmdbRepository {
  Future<List<MovieEntity>> getBillboardMovies({
    int page = 1,
  });

  Future<List<MovieEntity>> getUpcomingMovies({
    int page = 1,
  });

  Future<List<MovieEntity>> getPopularMovies({
    int page = 1,
  });

  Future<List<MovieEntity>> getTopRatedMovies({
    int page = 1,
  });

  Future<MovieEntity> getDetailMovieById(String id);

  Future<List<ActorsEntity>> getActorsMovie(String id);

  Future<List<MovieEntity>> searchMovie(String query);
}
