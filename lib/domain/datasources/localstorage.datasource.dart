import 'package:cinevision/domain/entities/movie.entity.dart';

abstract class LocalStorageDatasource {
  Future<bool> toggleFavorite(MovieEntity movie);
  Future<bool> isMovieFavorite(int id);
  Future<List<MovieEntity>> loadMoviesFavorite({
    int limit = 10,
    int offset = 0,
  });
}
