# cinevision

### Dev

1. Copiar el .env.templte y renombrarlo a .env
2. Agregar las variables de entorno de (https://www.themoviedb.org/settings/api)
3. Si hay cambios en las entidades hay que ejecutar el siguiente comando:
```
    $ flutter pub run build_runner build
```